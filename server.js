const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const app = express();

const utilisateur = require('./controllers/utilisateur');
const contact = require('./controllers/contact');

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

app.use(cors());

app.get('/', (req, res) => {
  res.send('Server running successfully ...');
})

app.get('/users', utilisateur.list);

app.get('/user/:id', utilisateur.getById);

app.post('/updateUserState', utilisateur.updateState);

app.post('/addUser', utilisateur.add);

app.get('/contacts/:id', contact.list);

app.post('/addContact', contact.add);

app.post('/updateContact', contact.update);

app.delete('/deleteContact/:id', contact.delete);

app.listen(process.env.PORT || 5000, () => {
  console.log("Server is running successfully");
});

