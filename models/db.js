const mongoose = require('mongoose'), Schema = mongoose.Schema;

const username = "kitolay";
const password = "P@ssw0rd";
const cluster = "Cluster0";
const dbname = "myFirstDatabase";

mongoose.connect('mongodb+srv://kitolay:M0depass@cluster0.stvjaa5.mongodb.net/contacts?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,       
      useUnifiedTopology: true
    }
  );
  
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
    console.log("Connected successfully");
});

const utilisateurSchema = new Schema({    
    username: String,
    motdepasse: String,
    userState: String
});

const contactSchema = new Schema({
  nom: String,
  prenoms: String,
  email: String,
  whatsapp: String,
  user_id: String
  
});


const Utilisateur = mongoose.model('utilisateurs', utilisateurSchema);
const Contact = mongoose.model('contacts', contactSchema);

module.exports = {
  Utilisateur: Utilisateur, 
  Contact: Contact
};


//module.exports = mongoose.model('Contact', contactSchema);