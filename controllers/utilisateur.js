const {Utilisateur} = require('../models/db.js');
const bcrypt = require('bcrypt');

exports.add = function(req, res){
	const utilisateur = new Utilisateur(req.body);
	const salt = bcrypt.genSalt(10);  

  try {
		res.header("Access-Control-Allow-Origin", "*");
		utilisateur.motdepasse = bcrypt.hashSync(utilisateur.motdepasse, 10);
		utilisateur.save();
		res.send(utilisateur);       
  } catch (error) {
			res.status(500).send(error);     
  }
};

exports.list = function(req, res){
  Utilisateur.find(function(err, utilisateurs) {
  try {
		res.header("Access-Control-Allow-Origin", "*");
		res.send(utilisateurs);       
      } catch (error) {
        res.status(500).send(error);     
      }  
  });
};

exports.getById = function(req, res){  
  Utilisateur.find({})
  .where('_id').equals(req.params.id)
  .exec(function (err, data) {
            if (err) {
                console.log(err);
                console.log('error returned');
                res.status(500).send({ error: 'Failed get User' });
            }

            if (!data) {
                res.status(403).send({ error: 'Forbidden' });
            }

            res.status(200).send(data);
  })
};

exports.updateState = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", true);
    Utilisateur.findOneAndUpdate(
        { _id: req.body.id },
        { $set:
			{
				userState: req.body.userState								
			} 
		},
        { new:true }).then((docs) => {
        if(docs) {           
           res.send("State Updated ");
        } else {
            res.send("State Updated KO serv");
        }
    }).catch((err)=>{
        res.send("Update Error");
    });      
};