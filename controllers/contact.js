const {Contact} = require('../models/db.js');

exports.list = function(req, res){  
  Contact.find({})
  .where('user_id').equals(req.params.id)
  .exec(function (err, data) {
            if (err) {
                console.log(err);
                console.log('error returned');
                res.status(500).send({ error: 'Failed get List' });
            }

            if (!data) {
                res.status(403).send({ error: 'Forbidden' });
            }

            res.status(200).send(data);
  })
};

exports.add = function(req, res){
	const contact = new Contact(req.body);
    try {
		    res.header("Access-Control-Allow-Origin", "*");
			contact.save();
			res.send(contact);       
      } catch (error) {
			  res.status(500).send(error);     
      }
};

exports.update = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", true);
    Contact.findOneAndUpdate(
        { _id: req.body.id },
        { $set:
			{
				nom: req.body.nom, 
				prenoms: req.body.prenoms,
				email: req.body.email,
				whatsapp: req.body.whatsapp				
			} 
		},
        { new:true }).then((docs) => {
        if(docs) {           
           res.send("Update ok ");
        } else {
            res.send("Update KO serv");
        }
    }).catch((err)=>{
        res.send("Update Error");
    });      
};

exports.delete = function(req, res){  	
    try {	
		    res.header("Access-Control-Allow-Origin", "*");
			const contact = Contact.findByIdAndDelete(req.params.id, function (err, docs) {
				if (err){
					res.status(400).send("Aucun contact trouvé !!!", err);
					console.log(err);
				}
				else{
					res.status(200).send("Contact supprimé");
					console.log("Deleted : ", docs);
				}
			});
      } catch (error) {
			  res.status(500).send(error);     
      }
}